# 考勤系统后台 RESTful APIs

## 访问项目链接
$ http://47.95.9.18/start/

## 开发环境部署

PyCharm + Python 3 + Django 2.0 + Django-Rest-Framework 3.8.2 

```bash
# 如果没安装 git，请先安装
$ git clone https://gitlab.com/Panda_216/attendance-system
$ cd back-end
# 如果没安装 pip3，请先安装
$ pip install -r requirement.txt -i https://pypi.douban.com/simple
# 运行后台服务器
$ python manage.py runserver
```

## 生产环境部署

Ubuntu16.04 + Nginx 1.10.3 + MySQL5.7.22 + Python3.5.2 + Django2.0.3

首先，在开发项目的根目录中导出 Python 依赖包列表。

```bash
$ pip freeze > requirement.txt
```

然后，将项目文件夹拷贝到服务器上，安装依赖。

```bash
$ pip install -r requirement.txt -i https://pypi.douban.com/simple
```

### 配置 uwsgi

安装 `pip install uwsgi`

uwsgi 配置文件在 `uwsgi/uwsgi.ini`

使用以下命令进行操作，需要在 `uwsgi.ini` 的目录中进行

| 操作 | 命令                        |
| ---- | --------------------------- |
| 启动 | ` uwsgi --ini uwsgi.ini`    |
| 停止 | ` uwsgi --stop uwsgi.pid`   |
| 重启 | ` uwsgi --reload uwsgi.pid` |

### 配置 Nginx

将 `uwsgi` 目录下的 `book.conf` 拷贝到 `/etc/nginx/conf.d/` 

将 `uwsgi/cert/` 目录及文件拷贝到 `/etc/nginx/`

重启 Nginx 即可生效 `service nginx restart`

## 参考资料

豆瓣图书 API https://developers.douban.com/wiki/?title=book_v2#get_isbn_book

Django + Uwsgi + Nginx 的生产环境部署 https://www.cnblogs.com/chenice/p/6921727.html

Django Rest Framework 官方网站 https://www.django-rest-framework.org/

Django 2.0 中文文档 https://docs.djangoproject.com/zh-hans/2.0/

DRF 学习资源 http://drf.jiuyou.info/#/
