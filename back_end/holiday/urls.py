from django.urls import path, include
from rest_framework.urlpatterns import format_suffix_patterns
from holiday import views


urlpatterns = [
    path('holidays/', views.HolidayList.as_view(), name="holidays"),
    path('holidays/<int:pk>', views.HolidayDetail.as_view(), name="holiday-detail"),
]

urlpatterns = format_suffix_patterns(urlpatterns)