from django.shortcuts import render

# Create your views here.
from holiday.models import Holiday
from holiday.serializers import HolidaySerializer
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from django.http import Http404


class HolidayList(APIView):
    def get(self, request, format=None):
        holidays = Holiday.objects.all()
        count = holidays.count()
        serializer = HolidaySerializer(holidays, many=True)
        return Response({"code": 0, "msg": "success", "count": count, "data": serializer.data})

    def post(self, request, format=None):
        serializer = HolidaySerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class HolidayDetail(APIView):
    """
    检索查看、更新或者删除一个holiday类型
    """
    def get_object(self, pk):
        try:
            return Holiday.objects.get(pk=pk)
        except Holiday.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        holiday = self.get_object(pk)
        serializer = HolidaySerializer(holiday)
        return Response({"code": 0, "msg": "success", "count": 1, "data": serializer.data})

    def put(self, request, pk, format=None):
        holiday = self.get_object(pk)
        serializer = HolidaySerializer(holiday, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        holiday = self.get_object(pk)
        holiday.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

