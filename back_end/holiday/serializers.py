from rest_framework import serializers
from holiday.models import Holiday


class HolidaySerializer(serializers.ModelSerializer):
    hid = serializers.ReadOnlyField()

    class Meta:
        model = Holiday
        fields = "__all__"

