from django.db import models

# Create your models here.


class Holiday(models.Model):
    """
    假期信息
    type 假期类型即名称 e.g 年假、事假
    status(0/1) 是否启用                 0:不启用  1:启用
    max_days    最长可修时间
    period(1/2/3)                      1:每周    2:每月   3:每年
    attachment(0/1)  是否需要附件        0:不需要   1:需要
    default(0/1)      是否是默认假期类型  0:不是     1:是
    """
    status = (
        ('0', "不启用"),
        ('1', "启用"),
    )

    period = (
        ('1', "每周"),
        ('2', "每月"),
        ('3', "每年"),
    )

    attachment = (
        ('0', "不需要"),
        ('1', "需要"),
    )

    default = (
        ('0', "不是"),
        ('1', "是"),
    )

    hid = models.AutoField(primary_key=True, verbose_name='假期类型ID')
    type = models.CharField(max_length=32, unique=True, verbose_name='假期类型')
    status = models.CharField(max_length=32, choices=status, verbose_name='是否启用', default="启用")
    period = models.CharField(max_length=32, choices=period, verbose_name='周期', default="每月")
    attachment = models.CharField(max_length=32, choices=attachment, verbose_name='是否需要附件', default="不需要")
    default = models.CharField(max_length=32, choices=default, verbose_name='是否是默认假期类型', default="不是")
    max_days = models.IntegerField(default=1, verbose_name="最长休假时间")

    def __str__(self):
        return self.type

    class Meta:
        verbose_name_plural = '假期信息'


