from django.apps import AppConfig


class HolidayConfig(AppConfig):
    name = 'holiday'
    verbose_name = '假期信息管理及状态控制模块'
