"""back_end URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from account import urls
from holiday import urls


urlpatterns = [
    path('admin/', admin.site.urls),
    # 平台用户管理及权限管理
    path('', include('account.urls'), name="account"),
    # 假期类型及状态管理
    path('', include('holiday.urls'), name="holiday"),

    # DRF自动化文档
    # path('docs/', include_docs_urls(title='考勤系统文档')),

]

#urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

#if not settings.DEBUG:
#   urlpatterns += [url(r'^static/(?P<path>.*)$', serve, {'document_root': settings.STATIC_ROOT})]
