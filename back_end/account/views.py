# Create your views here.
from account.models import Account, PersonalHoliday, RequestForLeave
from account.serializers import AccountSerializer, PersonalHolidaySerializer, RequestForLeaveSerializer
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from django.http import Http404


class AccountList(APIView):
    def get(self, request, format=None):
        accounts = Account.objects.all()
        count = accounts.count()
        serializer = AccountSerializer(accounts, many=True)
        return Response({"code": 0, "msg": "success", "count": count, "data": serializer.data})

    def post(self, request, format=None):
        serializer = AccountSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response({"code": 0, "msg": "添加成功"})
        return Response({"code": 1, "msg": "添加失败，不告诉你为什么"})


class AccountDetail(APIView):
    """
    检索查看、更新或者删除一个account
    """
    def get_object(self, pk):
        try:
            return Account.objects.get(pk=pk)
        except Account.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        account = self.get_object(pk)
        serializer = AccountSerializer(account)
        return Response({"code": 0, "msg": "success", "count": 1, "data": serializer.data})

    def put(self, request, pk, format=None):
        account = self.get_object(pk)
        serializer = AccountSerializer(account, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        account = self.get_object(pk)
        account.delete()
        return Response({"code": 0, "msg": "删我干嘛,呜呜"})


class Login(APIView):
    def post(self, request, format=None):
        work_num = request.POST.get('work_num', None)
        password = request.POST.get('password', None)
        message = "所有字段都必须填写"
        if work_num and password:  # 确保用户名和密码都不为空
            work_num = work_num.strip()
            try:
                account = Account.objects.get(work_num=work_num)
                if account.password == password:
                    print("success")
                    return Response({"code": 0, "msg": "success", "priority": account.priority})
                    # return Response({"msg": "success", "data": AccountSerializer(account)})
                else:
                    print("密码不正确")
                    return Response({"code": 1, "msg": "密码不正确", "priority": account.priority})
                    # return Response({"msg": "密码不正确"}, status=status.HTTP_400_BAD_REQUEST)
            except:
                print("用户不存在")
                return Response({"code": 1, "msg": "用户不存在，快跑去注册", "priority": account.priority})
                # return Response({"msg": "用户不存在"}, status=status.HTTP_400_BAD_REQUEST)


class PersonalHolidayList(APIView):
    def get(self, request, format=None):
        personal = PersonalHoliday.objects.all()
        count = personal.count()
        serializer = PersonalHolidaySerializer(personal, many=True)
        return Response({"code": 0, "msg": "success", "count": count, "data": serializer.data})

    def post(self, request, format=None):
        serializer = PersonalHolidaySerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class PersonalHolidayDetail(APIView):
    """
    检索查看、更新或者删除一个PersonalHoliday
    """
    def get_object(self, pk):
        try:
            return PersonalHoliday.objects.get(pk=pk)
        except PersonalHoliday.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        personal = self.get_object(pk)
        serializer = PersonalHolidaySerializer(personal)
        return Response({"code": 0, "msg": "success", "count": 1, "data": serializer.data})

    def put(self, request, pk, format=None):
        personal = self.get_object(pk)
        serializer = PersonalHolidaySerializer(personal, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        personal = self.get_object(pk)
        personal.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class RequestForLeaveList(APIView):
    def get(self, request, format=None):
        leave_request = RequestForLeave.objects.all()
        count = leave_request.count()
        serializer = RequestForLeaveSerializer(leave_request, many=True)
        return Response({"code": 0, "msg": "success", "count": count, "data": serializer.data})

    def post(self, request, format=None):
        serializer = RequestForLeaveSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class RequestForLeaveDetail(APIView):
    """
    检索查看、更新或者删除一个RequestForLeave
    """
    def get_object(self, pk):
        try:
            return RequestForLeave.objects.get(pk=pk)
        except PersonalHoliday.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        leave_request = self.get_object(pk)
        serializer = RequestForLeaveSerializer(leave_request)
        return Response({"code": 0, "msg": "success", "count": 1, "data": serializer.data})

    def put(self, request, pk, format=None):
        leave_request = self.get_object(pk)
        serializer = RequestForLeaveSerializer(leave_request, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def post(self, request, format=None):
        serializer = RequestForLeaveSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        leave_request = self.get_object(pk)
        leave_request.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
