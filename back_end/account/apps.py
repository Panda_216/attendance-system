from django.apps import AppConfig


class AccountConfig(AppConfig):
    name = 'account'
    verbose_name = '帐号信息管理及权限控制模块'

