from rest_framework import serializers
from account.models import Account, PersonalHoliday, RequestForLeave
from holiday.serializers import HolidaySerializer


class AccountSerializer(serializers.ModelSerializer):
    uid = serializers.ReadOnlyField()

    class Meta:
        model = Account
        fields = "__all__"


class PersonalHolidaySerializer(serializers.ModelSerializer):
    account = AccountSerializer(allow_null=True)
    holiday = HolidaySerializer(allow_null=True)

    class Meta:
        model = PersonalHoliday
        fields = "__all__"


class RequestForLeaveSerializer(serializers.ModelSerializer):
    account = AccountSerializer(allow_null=True)
    holiday = HolidaySerializer(allow_null=True)

    class Meta:
        model = RequestForLeave
        fields = "__all__"
