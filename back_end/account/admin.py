from django.contrib import admin
from account.models import Account, PersonalHoliday, RequestForLeave
# Register your models here.
admin.site.register(Account)
admin.site.register(PersonalHoliday)
admin.site.register(RequestForLeave)
