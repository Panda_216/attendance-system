from django.db import models
from holiday.models import Holiday

# Create your models here.


class Account(models.Model):
    """
    用户账号
    uid 是用来标识用户的唯一 ID
    username 是用户名，可以用来登录，是唯一的，只能使用英文字母、数字和下划线（_），长度为 6-24，如果用户不设置用户名，则默认生成32位用户名
    password 是密码，加密存储
    department 部门
    work_num 工号
    priority 优先级
    """
    department = (
        ('0', "软件学院"),
        ('1', "计算机学院"),
        ('2', "网络安全学院"),
        ('3', "人工智能学院")
    )

    priority = (
        ('0', "总经理"),
        ('1', "部门经理"),
        ('2', "普通员工")
    )

    uid = models.AutoField(primary_key=True, verbose_name='用户 ID')
    username = models.CharField(max_length=32, unique=True, verbose_name='用户名')
    password = models.CharField(max_length=128, null=True, default=None, verbose_name='密码')
    department = models.CharField(max_length=32, choices=department, verbose_name='部门', default="软件学院")
    work_num = models.CharField(max_length=128, unique=True, verbose_name='工号')
    priority = models.CharField(max_length=32, choices=priority, verbose_name='优先级', default="普通员工")

    def __str__(self):
        return self.work_num

    class Meta:
        verbose_name_plural = '用户账号'


class PersonalHoliday(models.Model):
    account = models.ForeignKey(
        'Account',
        on_delete=models.CASCADE,
        verbose_name="用户账号"
    )

    holiday = models.ForeignKey(
        'holiday.Holiday',
        on_delete=models.SET_NULL,
        null=True,
        verbose_name="假期类型"
    )

    remaining = models.IntegerField(verbose_name="该类型假期剩余时长")

    class Meta:
        verbose_name = "个人假期"
        verbose_name_plural = verbose_name

    def __str__(self):
        return self.account.username


class RequestForLeave(models.Model):
    """
    请假申请表存储表

    工号
    请假类型
    开始时间
    结束时间
    请假时长
    请假事由
    审批状态  0:通过 1:未通过
    """
    status = (
        ('0', "通过"),
        ('1', "未通过")
    )

    account = models.ForeignKey('Account',  on_delete=models.CASCADE, verbose_name="工号")
    holiday = models.ForeignKey('holiday.Holiday', on_delete=models.CASCADE, verbose_name="假期类型")

    start_time = models.DateField(verbose_name="开始时间")
    end_time = models.DateField(verbose_name="结束时间")
    length = models.IntegerField(verbose_name="请假时长")
    reason = models.CharField(max_length=480, verbose_name="请假原因")
    status = models.CharField(max_length=1, choices=status, default="未通过", verbose_name="审批状态")

    def __str__(self):
        return self.reason

    class Meta:
        verbose_name_plural = '请假审批表信息'
