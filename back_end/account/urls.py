from django.urls import path, include
from rest_framework.urlpatterns import format_suffix_patterns
from account import views


urlpatterns = [
    path('accounts/', views.AccountList.as_view(), name="accounts"),
    path('accounts/<int:pk>', views.AccountDetail.as_view(), name="account-detail"),

    path('login/', views.Login.as_view(), name="login"),

    path('p-holidays/', views.PersonalHolidayList.as_view(), name="personal-holidays"),
    path('p-holidays/<int:pk>', views.PersonalHolidayDetail.as_view(), name="personal-holiday-detail"),

    path('requests/', views.RequestForLeaveList.as_view(), name="requests"),
    path('requests/<int:pk>', views.RequestForLeaveDetail.as_view(), name="request-detail"),
]

urlpatterns = format_suffix_patterns(urlpatterns)