# Django 常用命令及操作

> 1. 创建应用后，需在 settings.py 文件中的 INSTALLED_APPS 数组内添加应用名称，如：'tutorial.apps.TutorialConfig'

| 命令                                       | 功能                         |
| ------------------------------------------ | ---------------------------- |
| `django-admin startproject [project name]` | 建立项目                     |
| `django-admin startapp [app name]`         | 建立应用                     |
| `python manage.py makemigrations`          | 创建数据库迁移文件           |
| `python manage.py migrate`                 | 将生成的迁移文件应用到数据库 |
| `python manage.py createsuperuser`         | 创建超级管理员               |
| `python manage.py collectstatic`           | 收集静态文件                 |
| `python manage.py runserver`               | 启动项目，默认监听`8000`端口 |

参考资料 http://drf.jiuyou.info/#/django/django%E5%91%BD%E4%BB%A4

# 数据库相关操作

建立项目数据库

```mysql
-- 建立数据库 --
-- 注意，数据库字符集使用 utf8mb4 --
DROP DATABASE IF EXISTS `restfulapis`;
CREATE DATABASE `restfulapis` CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
-- 添加数据库用户 --
GRANT ALL PRIVILEGES ON restfulapis.* TO restfulapis@'%' IDENTIFIED BY '52B32D6042BACB07C74B2F3E7E1C8E6E';
FLUSH PRIVILEGES;
```

数据库建立完成，并将数据模型迁移到数据库之后，需要初始化数据库中的数据

```bash
$ python manage.py loaddata initial.json
```

