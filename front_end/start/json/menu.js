{
  "code": 0
  ,"msg": ""
  ,"data": [
  {
    "title": "员工管理"
    ,"icon": "layui-icon-user"
    ,"list": [{
      "title": "员工列表"
      ,"jump": "/members/list"
    }, {
      "name": "homepage1"
      ,"title": "新添员工"
      ,"jump": "/members/add"
    }]
  },
  {
    "title": "假期管理"
    ,"icon": "layui-icon-template"
    ,"list": [{
      "title": "假期类型"
      ,"jump": "holiday/h_type/list"
    }, {
      "name": "homepage1"
      ,"title": "假期列表"
      ,"jump": "holiday/h_list/show_list"
    }]
  },
  {
    "title": "请假管理"
    ,"icon": "layui-icon-home"
    ,"list": [{
      "title": "审批列表"
      ,"jump": "leave/leave_list"
    }, {
      "name": "askleave"
      ,"title": "假期申请"
      ,"jump": "leave/askleave"
    }]
  }
  ]
}