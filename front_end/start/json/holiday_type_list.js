{
  "code": 0
  ,"msg": ""
  ,"count": "3"
  ,"data": [
    {
      "id":"1"
      ,"holiday": "年假"
      ,"status": 1
      ,"max_time": 10
      ,"period": 1
      ,"attachment": 1
      ,"default": 0
    },
    {
      "id": "2"
      ,"holiday": "调休"
      ,"status": 0
      ,"max_time": 5
      ,"period": 2
      ,"attachment": 0
      ,"default": 0
    },
    {
      "id":"3"
      ,"holiday": "奖励假"
      ,"status": 1
      ,"max_time": 12
      ,"period": 3
      ,"attachment": 1
      ,"default": 0
    }
  ]
}